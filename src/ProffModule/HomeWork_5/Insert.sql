INSERT INTO clients(username, phone) VALUES ('Andrey','89995553344');
INSERT INTO clients(username, phone) VALUES ('Irina','89994443377');
INSERT INTO clients(username, phone) VALUES ('Sergey','87777777777');

INSERT INTO flowers(flower_name, flower_price) VALUES ('Роза',100);
INSERT INTO flowers(flower_name, flower_price) VALUES ('Лилия',50);
INSERT INTO flowers(flower_name, flower_price) VALUES ('Ромашка',20);

INSERT INTO orders(client, flower, amount, order_date) VALUES (1,3,20,'2022-12-05');
INSERT INTO orders(client, flower, amount, order_date) VALUES (2,3,30,'2022-12-16');
INSERT INTO orders(client, flower, amount, order_date) VALUES (3,3,40,'2023-01-20');
INSERT INTO orders(client, flower, amount, order_date) VALUES (1,2,50,'2022-04-01');
INSERT INTO orders(client, flower, amount, order_date) VALUES (2,2,60,'2022-06-25');
INSERT INTO orders(client, flower, amount, order_date) VALUES (3,2,70,'2022-07-30');