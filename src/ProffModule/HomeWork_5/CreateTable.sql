CREATE TABLE clients
(
    id       serial primary key,
    username varchar(30) not null,
    phone    varchar(11) not null
);

CREATE TABLE flowers
(
    id           serial primary key,
    flower_name  varchar(20) not null,
    flower_price integer     not null,
    CHECK ( flower_price > 0 AND flower_price <= 1000 )
);

CREATE TABLE orders
(
    id         serial primary key,
    client     integer REFERENCES clients (id),
    flower     integer REFERENCES flowers (id),
    amount     integer NOT NULL,
    order_date date
);
