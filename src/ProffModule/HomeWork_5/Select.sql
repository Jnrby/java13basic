/*
ДЗ 5 задание 1
По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
*/

SELECT o.order_date              "Дата заказа",
       c.username                Имя,
       c.phone                   Телефон,
       f.flower_name             Цветы,
       f.flower_price            "Цена за шт.",
       o.amount                  Количество,
       f.flower_price * o.amount "Общая стоимость"
FROM orders o
         INNER JOIN clients c on c.id = o.client
         INNER JOIN flowers f on f.id = o.flower
WHERE o.id = 5;

/*
ДЗ 5 задание 2
Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
*/

SELECT o.order_date              "Дата заказа",
       c.username                Имя,
       f.flower_name             Цветы,
       o.amount                  Количество,
       f.flower_price * o.amount "Общая стоимость"
FROM clients c
         INNER JOIN orders o ON o.client = c.id
         INNER JOIN flowers f ON f.id = o.flower
WHERE c.id = 3
  AND o.order_date >= date_trunc('month', now())
  AND o.order_date < date_trunc('day', now()) + interval '1 day';


/*
ДЗ 5 задание 3
Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
 */

SELECT f.flower_name Цветы,
       o.amount      Количество
FROM orders o
         INNER JOIN flowers f on f.id = o.flower
WHERE "amount" = (SELECT MAX("amount") from orders);

/*
 ДЗ 5 задание 4
 Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
 */

SELECT SUM(f.flower_price * o.amount)
from orders o
         INNER JOIN flowers f on f.id = o.flower

